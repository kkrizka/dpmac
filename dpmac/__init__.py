import dash
import dash_auth
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from dpmac import main, config

#
# Configuration
cfg=config.Config('config.json')

#
# Main page
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
auth = dash_auth.BasicAuth(
    app, {}
    )

cfg.auth=auth

app.layout = html.Div(children=[
    dcc.Location(id='url', refresh=False),
    html.Div(children=[
        html.B('Navigation'),
        html.Ul(children=[
            html.Li(children=dcc.Link('Google',href='/google')),
            html.Li(children=dcc.Link('Yahoo',href='/yahoo')),
            html.Li(children=dcc.Link('Config',href='/config'))
            ]),
        ]),
    html.Div(id='page-content',children='App is loading...')
    ])

#
# Routes

page_google= main.MainPage('google', app, 'ping www.google.com')
page_yahoo = main.MainPage('yahoo' , app, 'ping www.yahoo.com' )

page_config= config.ConfigPage(cfg, app)

@app.callback(Output('page-content', 'children'),
                  [Input('url', 'pathname')])
def display_page(pathname):
    global page_google, page_yahoo

    if pathname == '/' or pathname=='/google':
        return page_google.layout
    elif pathname=='/yahoo':
        return page_yahoo .layout
    elif pathname=='/config':
        return page_config .layout
    return '404'
