import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import os
import json

class Config:    
    def __init__(self, confpath):
        self.confpath=confpath

        self.__auth=None

        # Configs
        self.__username='admin'
        self.__password='pixelssuck'

        if os.path.exists(confpath):
            self.load()

    def load(self):
        with open(self.confpath) as json_file:
            data = json.load(json_file)
            self.username=data.get('username',self.username)
            self.password=data.get('password',self.password)

    def save(self):
        with open(self.confpath,'w') as json_file:
            json.dump({
                'username': self.username,
                'password': self.password
                }, json_file)

    #
    # Tools
    @property
    def auth(self):
        return self.__auth

    @auth.setter
    def auth(self, newauth):
        self.__auth=newauth
        self.update_auth()

    def update_auth(self):
        if self.__auth==None:
            return
        self.__auth._users={self.username: self.password}

    #
    # Configurations
    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, username):
        self.__username=username
        self.update_auth()

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password=password
        self.update_auth()
        
class ConfigPage:
    def __init__(self, cfg, app):
        self.cfg=cfg

        self.layout = html.Div(children=[
            html.Div(id='div_passbtnplaceholder'),
            html.B('Change Password'),
            html.Table(children=[
                html.Tr(children=[
                    html.Td('Password:'),
                    html.Td(dcc.Input(id='cfg_pass1',type='password'))
                    ]),
                html.Tr(children=[
                    html.Td('Confirm Password:'),
                    html.Td(dcc.Input(id='cfg_pass2',type='password'))
                    ])
                ]),
            html.Button('Update', id='cfg_passbtn', n_clicks=0, disabled=True),
            html.Span('',id='cfg_message')
            ])

        app.callback([
            Output('cfg_message', 'children'),
            Output('cfg_passbtn', 'disabled'),
            ],[
            Input('cfg_pass1','value'),
            Input('cfg_pass2','value')
            ])(self.check_password)

        app.callback(
            Output('div_passbtnplaceholder', 'children'),
            [
            Input('cfg_passbtn','n_clicks')
            ],[
            State('cfg_pass1','value')
            ],prevent_initial_call=True)(self.save_password)

    def check_password(self,pass1,pass2):
        """
        Check if new password is valid
        """

        if pass1!=pass2:
            return 'Password confirmation does not match.',True
        return '',False

    def save_password(self,n_clicks,pass1):
        """
        Save password to file
        """
        print(pass1)
        self.cfg.password=pass1
        self.cfg.save()
        return ''
