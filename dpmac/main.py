import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly

import subprocess
import tempfile

class Process:
    def __init__(self,cmd):
        self.cmd=cmd
        self.proc=None

        (fd, filename) = tempfile.mkstemp()

        self.path       = filename
        self.path_stdout= fd

    @property
    def running(self):
        if self.proc==None:
            return False
        return self.proc.poll()==None

    @property
    def returncode(self):
        if self.proc==None:
            return None
        return self.proc.poll()
    
    def run(self):
        if self.running:
            # Already running..
            return

        # Prepare place to store the output
        self.fh_stdout=open(self.path_stdout,'w')

        # Start the process
        self.proc=subprocess.Popen(self.cmd, stdout=self.fh_stdout)

    def stop(self):
        if not self.running:
            # Not running
            return

        self.proc.terminate()
        
class MainPage:
    def __init__(self, name, app, command):
        self.proc=Process(command.split())

        self.layout = html.Div(children=[
            html.Button(id=f'{name}_button_run' , n_clicks=0, children="Run" , disabled=False),
            html.Button(id=f'{name}_button_stop', n_clicks=0, children="Stop", disabled=True ),
            html.Div(id=f'{name}_div_status'),
            html.Pre(id=f'{name}_div_output'),
            html.Div(id=f'{name}_div_runplaceholder',style={'display':'none'}),
            html.Div(id=f'{name}_div_stopplaceholder',style={'display':'none'}),
            dcc.Interval(
                id=f'{name}_update-interval',
                interval=1*1000,
                n_intervals=0)
            ])

        app.callback(Output(f'{name}_div_runplaceholder' ,'children'),[Input(f'{name}_button_run' , 'n_clicks')], prevent_initial_call=True)(self.run_control )
        app.callback(Output(f'{name}_div_stopplaceholder','children'),[Input(f'{name}_button_stop', 'n_clicks')], prevent_initial_call=True)(self.stop_control)
        app.callback(
            [Output(f'{name}_div_status','children'),Output(f'{name}_div_output', 'children')],
            [Input(f'{name}_update-interval', 'n_intervals')]
            )(self.update_monitoring)

        #
        # This callback updates the button states
        app.clientside_callback("""
          function(status) {
            var drun = status=="Running"
            var dstop= status!="Running"
            return [drun,dstop]
          }
         """,
                                    [Output(f'{name}_button_run','disabled'),Output(f'{name}_button_stop','disabled')],
                                    [Input(f'{name}_div_status','children')])     

    def run_control(self,button):
        """
        This callback is responsible for starting the process.
        Check are made to ensure that it is not double started
        """

        self.proc.run()

    def stop_control(self,button):
        """
        This callback is responsible for stopping the process.
        """

        self.proc.stop()

    def update_monitoring(self,button):
        """
        This callback is responsible for retrieving the status of
        the process and sending it back.
        """

        # Determine status text
        status='Stopped'
        if self.proc.running:
            status='Running'
        elif self.proc.returncode==0:
            status='Done'
        elif self.proc.returncode!=None:
            status='Error'

        # Determine the current output
        output=''
        if status in ['Running','Done','Error']:
            fh=open(self.proc.path,'r')
            output=''.join([line for line in fh])
            fh.close()

        return status, output            

        




