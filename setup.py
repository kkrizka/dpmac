#!/usr/bin/env python

from distutils.core import setup

setup(
    name='dpmac',
    version='0.1',
    description='Dash Program Monitoring and Control',
    author='Karol Krizka',
    author_email='kkrizka@cern.ch',
    url='https://gitlab.cern.ch/kkrizka/dpmac',
    packages=['dpmac'],
    scripts=['run_dpmac.py'],
    install_requires=[
        'requests',
        'dash',
        'dash_auth'
        ],
    )
